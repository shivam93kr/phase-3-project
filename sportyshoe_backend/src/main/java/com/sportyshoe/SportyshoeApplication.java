package com.sportyshoe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@ComponentScan({ "com.spring.controller", "com.spring.util" })
@EntityScan("com.spring.model")
@EnableJpaRepositories("com.spring.repository")
@CrossOrigin(origins = "http://localhost:4201")
public class SportyshoeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SportyshoeApplication.class, args);
	}
}
