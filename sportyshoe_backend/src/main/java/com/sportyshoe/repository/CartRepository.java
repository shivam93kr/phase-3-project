package com.sportyshoe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sportyshoe.model.Cart;

@Repository
@Transactional
public interface CartRepository extends JpaRepository<Cart, Long> {

	List<Cart> findByEmail(String email);

	Cart findByBufcartIdAndEmail(int bufcartId, String email);

	void deleteByBufcartIdAndEmail(int bufcartId, String email);

	List<Cart> findAllByEmail(String email);

	List<Cart> findAllByOrderId(int orderId);
}
